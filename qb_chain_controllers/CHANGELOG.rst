^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package qb_chain_control
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.0.0 (2022-07-06)
------------------

2.2.3 (2021-11-05)
------------------

2.2.2 (2021-08-30)
------------------

2.2.1 (2021-08-27)
------------------

2.2.0 (2021-08-26)
------------------
* Fixed joint limits and default robot movement.
* Minor changes
* QBADV-56 Added a topic to notify the motion request
* Fixed an error that prevents the usage of WAYPOINT modality. Added .yaml file for parsing wayoints.
* Update license
* Fix plugin manifests installation
* Removed some anusefull comments
* Fixed some errors in wrist kinematic and added the last kinematic config(arm_v2).
* Wrist Kinematic implemented.
* Added wrist files to implement the corresponding kinematic (not implemented yet).
* Changed the names of the calsses according to robot configurations described in the guide (qbmove KIT 23/12/2020). for 'arm2' used the library 'trac_ik' for solving inverse kinematic problem.
* fixed some minor bugs
* Ignored the files used by VisualStudio Code.
* Added the scripts for 2-axes wrist implementation. The project compiles but the inverse kinematic equations are wrong.
* Added a third kinematic configuration. The open chain kinematics are named arm1 and arm 2.
* added kinematic controller for arm1 configuration
* Added arm3 kinematic controller and custom message to set velocity and stiffness togheter with end-effector position
* Base class (KitKinematicController) is now less specific. Removed also some hardcoded parts.
* First steps towards class separation. Functioning at this stage but no virtual functions implemented.
* Changed class name (DeltaKinematicController -> KitKinematicController).
* End-effector offset imposed only for wp. In other cases the offset is zero.

2.1.1 (2019-06-11)
------------------
* Fix dependencies

2.1.0 (2019-05-28)
------------------
* Add delta controller
