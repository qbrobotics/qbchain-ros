/***
 *  Software License Agreement: BSD 3-Clause License
 *
 *  Copyright (c) 2016-2021, qbrobotics®
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 *  following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 *  USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <qb_chain_controllers/delta_kinematic_controller.h>

using namespace qb_chain_controllers;

DeltaKinematicController::DeltaKinematicController() {
  link_length_ = 0.09;
  kinematic_config_ = "delta";
};
DeltaKinematicController::~DeltaKinematicController() {};

bool DeltaKinematicController::forwardKinematics(const std::vector<double> &motor_joints, geometry_msgs::Point &ee_pose) {
  double R_motors = 0.1;  // radius from the center of the motors plane to each motor [meters]
  double R_ee = 0.0455;  // radius from the center of the end-effector plane to each arm [meters]
  double l_motors = link_length_;  // upper arm length [meters]
  double l_ee = 0.156;  // forearm length [meters]
  double phi_1 = 0;  // motor 1 angular position in the motors plane (front/central)
  double phi_2 = 2*M_PI/3;  // motor 2 angular position in the motors plane (behind/left)
  double phi_3 = 4*M_PI/3;  // motor 3 angular position in the motors plane (behind/right)
  double theta_1 = motor_joints.at(0) - 1;
  double theta_2 = motor_joints.at(1) - 1;
  double theta_3 = motor_joints.at(2) - 1;
  double R = R_motors - R_ee;

  // The forward kinematics for a 3-limb-delta structure comes from the intersection of the three spheres with radius
  // equals to the forearm and centered at the end of each upper limb.
  // Actually several assumptions are taken to simplify the system of equations, e.g. even if it is not true, we can
  // consider all the forearms to be linked in the center of the end-effector plane and use "R = R_motors - R_ee".
  //
  // The generic system is as follows:
  //
  //  { (x-[cos(phi_1)*(l_motors*cos(theta_1)+R)])^2 + (y-[-sin(phi_1)*(l_motors*cos(theta_1)+R)])^2 + (z-[-l_motors*sin(theta_1)])^2 = l_ee^2
  //  { (x-[cos(phi_2)*(l_motors*cos(theta_2)+R)])^2 + (y-[-sin(phi_2)*(l_motors*cos(theta_2)+R)])^2 + (z-[-l_motors*sin(theta_2)])^2 = l_ee^2
  //  { (x-[cos(phi_3)*(l_motors*cos(theta_3)+R)])^2 + (y-[-sin(phi_3)*(l_motors*cos(theta_3)+R)])^2 + (z-[-l_motors*sin(theta_3)])^2 = l_ee^2
  //
  // which can have in general 0, 1 or 2 solutions.
  // Note that if 2 solutions exist, only one is feasible.

  // eq_1: (x-x_1)^2 + (y)^2 + (z-z_1)^2 == l_ee^2  (phi_1 is hardcoded to simplify the computation)
  double x_1 = l_motors*cos(theta_1) + R;
  double z_1 = -l_motors*sin(theta_1);

  // eq_2: (x-x_2)^2 + (y-y_2)^2 + (z-z_2)^2 == l_ee^2
  double x_2 = std::cos(phi_2)*(l_motors*cos(theta_2) + R);
  double y_2 = -std::tan(phi_2)*x_2;
  double z_2 = -l_motors*sin(theta_2);

  // eq_3: (x-x_3)^2 + (y-y_3)^2 + (z-z_3)^2 == l_ee^2
  double x_3 = std::cos(phi_3)*(l_motors*cos(theta_3) + R);
  double y_3 = -std::tan(phi_3)*x_3;
  double z_3 = -l_motors*sin(theta_3);

  // "eq_4 = eq_1-eq2" and "eq_5 = eq1-eq3" produce two linear equations which are more easily to be solved
  //  { eq_4 - k_1*eq_5 --> x = (a_1*z + b_1)/d
  //  { eq_4 - k_2*eq_5 --> y = (a_2*z + b_2)/d
  double w_1 = std::pow(x_1,2) + std::pow(z_1,2);
  double w_2 = std::pow(x_2,2) + std::pow(y_2,2) + std::pow(z_2,2);
  double w_3 = std::pow(x_3,2) + std::pow(y_3,2) + std::pow(z_3,2);

  double d = (x_1-x_2)*y_3 - (x_1-x_3)*y_2;
  if (d == 0) {
    return false;
  }

  double a_1 = y_2*(z_1-z_3) - y_3*(z_1-z_2);
  double b_1 = (y_3*(w_1-w_2) - y_2*(w_1-w_3))/2;
  double a_2 = -(x_1-x_3)*(z_1-z_2)+(x_1-x_2)*(z_1-z_3);
  double b_2 = ((x_1-x_3)*(w_1-w_2)-(x_1-x_2)*(w_1-w_3))/2;

  // Now we can solve for z by substitution, e.g. in eq_1
  double a = std::pow(a_1,2) + std::pow(a_2,2) + std::pow(d,2);
  double b = 2*(a_1*b_1 + a_2*b_2 - z_1*std::pow(d,2) - x_1*a_1*d);
  double c = std::pow(b_1,2) + std::pow(b_2,2) + std::pow(x_1,2)*std::pow(d,2) + std::pow(z_1,2)*std::pow(d,2) - 2*x_1*b_1*d - std::pow(l_ee,2)*std::pow(d,2);
  double delta = std::pow(b,2) - 4*a*c;
  if (delta < 0) {
    return false;
  }

  ee_pose.z = -0.5*(b-std::sqrt(delta))/a;
  ee_pose.x = (a_1*ee_pose.z + b_1)/d;
  ee_pose.y = -(a_2*ee_pose.z + b_2)/d;
  return true;
}

bool DeltaKinematicController::inverseKinematics(const geometry_msgs::Point &ee_pose, std::vector<double> &joint_positions) {
  double R_motors = 0.1;  // radius from the center of the motors plane to each motor [meters]
  double R_ee = 0.0455;  // radius from the center of the end-effector plane to each arm [meters] --> !!!! PREVIOUSLY 0.0375 !!!! <-- 
  double l_motors = link_length_;  // upper arm length [meters]
  double l_ee = 0.156;  // forearm length [meters]
  double phi_1 = 0;  // motor 1 angular position in the motors plane (behind/right)
  double phi_2 = 2*M_PI/3;  // motor 2 angular position in the motors plane (front/central)
  double phi_3 = 4*M_PI/3;  // motor 3 angular position in the motors plane (behind/left)
  double R = R_motors - R_ee;
  joint_positions.resize(3);

  auto arm_ik = [&](const geometry_msgs::Point &ee_pose, const double &phi, double &joint_position) {
    auto acos_safe = [](const double &num, const double &den, double &angle) {
      angle = std::acos(num/den);  // it may be NaN
      return std::abs(num) < std::abs(den);
    };

    double x = std::cos(phi)*ee_pose.x + std::sin(phi)*ee_pose.y - R;
    double y = -std::sin(phi)*ee_pose.x + std::cos(phi)*ee_pose.y;
    double z = ee_pose.z;
    double theta_1, theta_2;
    if (!acos_safe(y, l_ee, theta_1) || !acos_safe((std::pow(x,2) + std::pow(y,2) + std::pow(z,2) - std::pow(l_motors,2) - std::pow(l_ee,2)), (2*l_motors*l_ee*std::sin(theta_1)), theta_2)) {
      return false;
    }
    double c_1 = l_ee*std::cos(theta_2)*std::sin(theta_1) + l_motors;
    double c_2 = l_ee*std::sin(theta_2)*std::sin(theta_1);
    joint_position = -(std::atan2(-c_2*x + c_1*z, c_1*x + c_2*z) - 1);  // includes motor offset
    return true;
  };

  return arm_ik(ee_pose, phi_1, joint_positions.at(0)) && arm_ik(ee_pose, phi_2, joint_positions.at(1)) && arm_ik(ee_pose, phi_3, joint_positions.at(2));
}

bool DeltaKinematicController::kitStatePublisher(const std::vector<double> &motor_joints, const geometry_msgs::Point &ee_pose) {
  double R_motors = 0.1;  // radius from the center of the motors plane to each motor [meters]
  double R_ee = 0.0455;  // radius from the center of the end-effector plane to each arm [meters]
  double l_motors = link_length_;  // upper arm length [meters]
  double l_ee = 0.156;  // forearm length [meters]
  double phi_1 = 0;  // motor 1 angular position in the motors plane (front/central)
  double phi_2 = 2*M_PI/3;  // motor 2 angular position in the motors plane (behind/left)
  double phi_3 = 4*M_PI/3;  // motor 3 angular position in the motors plane (behind/right)
  double theta_1 = motor_joints.at(0) - 1;
  double theta_2 = motor_joints.at(1) - 1;
  double theta_3 = motor_joints.at(2) - 1;

  sensor_msgs::JointState msg;
  std::vector<double> phi = {phi_1, phi_2, phi_3};
  std::vector<double> theta = {theta_1, theta_2, theta_3};
  for (int i=0; i<3; i++) {
    std::string base_name = "delta_qbmove_" + std::to_string(i+1);

    geometry_msgs::Point upperarm_pose;  // upperarm_poses: [cos(phi_i)*(l_motors*cos(theta_i)+R_motors) -sin(phi_i)*(l_motors*cos(theta_i)+R_motors) -l_motors*sin(theta_i)]
    upperarm_pose.x = cos(phi.at(i))*(l_motors*cos(theta.at(i))+R_motors);
    upperarm_pose.y = sin(phi.at(i))*(l_motors*cos(theta.at(i))+R_motors);
    upperarm_pose.z = -l_motors*sin(theta.at(i));

    geometry_msgs::Point forearm_pose;  // forearm_poses: [cos(phi_i)*l_ee -sin(phi_i)*l_ee 0] + ee_pose
    forearm_pose.x = cos(phi.at(i))*R_ee + ee_pose.x;
    forearm_pose.y = sin(phi.at(i))*R_ee + ee_pose.y;
    forearm_pose.z = 0 + ee_pose.z;

    geometry_msgs::Point diff_pose;
    diff_pose.x = forearm_pose.x - upperarm_pose.x;
    diff_pose.y = forearm_pose.y - upperarm_pose.y;
    diff_pose.z = forearm_pose.z - upperarm_pose.z;

    geometry_msgs::Point forearm_to_upperarm_vector;
    forearm_to_upperarm_vector.x = std::cos(-phi.at(i))*diff_pose.x + -std::sin(-phi.at(i))*diff_pose.y;
    forearm_to_upperarm_vector.y = std::sin(-phi.at(i))*diff_pose.x + std::cos(-phi.at(i))*diff_pose.y;
    forearm_to_upperarm_vector.z = diff_pose.z;

    double pitch = -std::atan2(forearm_to_upperarm_vector.z, -forearm_to_upperarm_vector.x);
    double yaw = std::atan2(forearm_to_upperarm_vector.y, std::hypot(forearm_to_upperarm_vector.x, forearm_to_upperarm_vector.z));
    msg.name.push_back(base_name + "_free_down_joint");
    msg.name.push_back(base_name + "_free_l_joint");
    msg.position.push_back(pitch);
    msg.position.push_back(yaw);
  }
  msg.header.stamp = ros::Time::now();
  free_joint_state_publisher_.publish(msg);

  geometry_msgs::TransformStamped transform;
  transform.header.frame_id = "delta_base_frame_link";
  transform.header.stamp = ros::Time::now();
  transform.child_frame_id = "delta_ee_frame_link";
  transform.transform.translation.x = ee_pose.x;
  transform.transform.translation.y = ee_pose.y;
  transform.transform.translation.z = ee_pose.z;
  transform.transform.rotation.w = 1;  // others are 0
  tf_broadcaster_.sendTransform(transform);
  return true;
}

PLUGINLIB_EXPORT_CLASS(qb_chain_controllers::DeltaKinematicController, controller_interface::ControllerBase)