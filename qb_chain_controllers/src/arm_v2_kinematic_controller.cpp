/***
 *  Software License Agreement: BSD 3-Clause License
 *  
 *  Copyright (c) 2016-2021, qbrobotics®
 *  All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 *  following conditions are met:
 *  
 *  * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *  
 *  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *  
 *  * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 *  USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <qb_chain_controllers/arm_v2_kinematic_controller.h>

using namespace qb_chain_controllers;

ArmV2KinematicController::ArmV2KinematicController()
  : ik_solver_("arm_v2_base_frame_link", "arm_v2_ee_frame_link", "/qbarm_v2/robot_description", 0.005, 1e-5),
    pos_tol_ (1e-5, 1e-5, 1e3),
    ang_tol_ (1e3, 1e3, 1e3),
    tolerances_ (KDL::Twist(pos_tol_, ang_tol_)),
    roll_(0.) {
      kinematic_config_ = "arm_v2";
      valid_ = ik_solver_.getKDLChain(chain_);
      target_poses_and_roll_sub_ = node_handle_control_.subscribe(kinematic_config_ + "_controller/target_poses_and_roll", 1, &ArmV2KinematicController::targetPosesAndRollCallback, this);
      q_.resize(2);
      q_(0) = 0.;
      q_(1) = 0.;
};

ArmV2KinematicController::~ArmV2KinematicController() {};

bool ArmV2KinematicController::forwardKinematics(const std::vector<double> &motor_joints, geometry_msgs::Point &ee_pose) {
  if(!valid_) {
    ROS_ERROR_STREAM("Invalid robot model found.");
    return false;
  }
  // variables for solving forward kinematic
  KDL::JntArray joints_positions(2);
  joints_positions(0) = motor_joints.at(0);
  joints_positions(1) = motor_joints.at(1);
  KDL::Frame end_effector_pose;
  KDL::ChainFkSolverPos_recursive fk_solver(chain_);
  int is_fk_ok = fk_solver.JntToCart(joints_positions, end_effector_pose);
  if(is_fk_ok < 0) {
    ROS_WARN_STREAM("Forward Kinematic has produced an invalid result.");
    return false;
  }
  ee_pose.x = end_effector_pose.p(0);
  ee_pose.y = end_effector_pose.p(1);
  ee_pose.z = end_effector_pose.p(2);
  return true;
};

bool ArmV2KinematicController::inverseKinematics(const geometry_msgs::Point &ee_pose, std::vector<double> &joint_positions) {
  if(!valid_) {
    ROS_ERROR_STREAM("Invalid robot model found.");
    return false;
  }

  KDL::Vector target_pos (ee_pose.x, ee_pose.y, ee_pose.z);
  KDL::Frame ee_target_pose(target_pos);
  KDL::JntArray result;
  int is_found = ik_solver_.CartToJnt(q_, ee_target_pose, result, tolerances_); 
  if (type_of_robot_movement_ == "moveL") {
    int number_of_cycles = 0;
    while (!isValidSolution(result, ee_target_pose)) {
      if(++number_of_cycles >= 25) {
        ROS_WARN_STREAM("No valid solution found with moveL. Try with moveJ or change robot start position (using rviz). ");
        return false;
      }
      is_found = ik_solver_.CartToJnt(q_, ee_target_pose, result, tolerances_); 
    }
  }
  if(is_found < 0) {
    ROS_WARN_STREAM("Kinematic inversion has produced a not acceptable result");
    return false;
  }

  joint_positions.resize(3);
  // joint 1 and 2 are controlled with ik results
  joint_positions.at(0) = result(0);
  joint_positions.at(1) = result(1);
  // joint 3 is controlled with roll angle specified by the user
  joint_positions.at(2) = result(0) + result(1) - roll_; // roll is expressed in global reference frame
    
  q_(0) = joint_positions.at(0);
  q_(1) = joint_positions.at(1);
  return true;
};

void ArmV2KinematicController::targetPosesAndRollCallback(const qb_chain_msgs::MoveAndRotateChain &qb_msg) {
  qb_chain_msgs::MoveChain qb_move_chain = qb_msg.move_robot;
  roll_ = qb_msg.roll; // roll angle in global reference frame
  targetPosesCallback(qb_move_chain);
};

bool ArmV2KinematicController::isValidSolution(KDL::JntArray jointVariables, KDL::Frame end_effector_pose) {
  double q1 = jointVariables(0);
  double q2 = jointVariables(1);

  double y =  end_effector_pose.p(1);
  bool condition = (((y < 0) && (q2 > 0)) || ((y > 0) && (q2 < 0)));
  return condition;
}
PLUGINLIB_EXPORT_CLASS(qb_chain_controllers::ArmV2KinematicController, controller_interface::ControllerBase)