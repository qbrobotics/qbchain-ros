/***
 *  Software License Agreement: BSD 3-Clause License
 *  
 *  Copyright (c) 2016-2021, qbrobotics®
 *  All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 *  following conditions are met:
 *  
 *  * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *  
 *  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *  
 *  * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 *  USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <qb_chain_controllers/arm_v1_kinematic_controller.h>

using namespace qb_chain_controllers;

ArmV1KinematicController::ArmV1KinematicController() {
  link_length_ = 0.09; // [m]
  kinematic_config_ = "arm_v1"; 
};
ArmV1KinematicController::~ArmV1KinematicController() {};

bool ArmV1KinematicController::forwardKinematics(const std::vector<double> &motor_joints, geometry_msgs::Point &ee_pose) {
  //params
  double l1 = link_length_; 
  double l2 = link_length_;
  double l3 = link_length_;

  double q1 = motor_joints.at(0);
  double q2 = motor_joints.at(1);
  double q3 = motor_joints.at(2);

  double tmp = l1 + l2*cos(q2) + l3*cos(q2 + q3);
  ee_pose.x = tmp*cos(q1);
  ee_pose.y = tmp*sin(q1);
  ee_pose.z = l2*sin(q2) + l3*sin(q2+q3);
  return true;
};

bool ArmV1KinematicController::inverseKinematics(const geometry_msgs::Point &ee_pose, std::vector<double> &joint_positions) {
  // this function is not-parametric, and it is evaluated considering the distance between motors shaft of 0.09 [m]
  double arm_length = std::sqrt(std::pow(ee_pose.x,2) + std::pow(ee_pose.y,2) + std::pow(ee_pose.z,2));
  if (arm_length < link_length_ || arm_length > 3*link_length_) {
    ROS_WARN("Robot cannot reach the imposed position in the cartesian space");
    return false;
  }
  joint_positions.resize(3);
  joint_positions.at(0) = atan(ee_pose.y/ee_pose.x);
  if (joint_positions.at(0) < - M_PI/2 || joint_positions.at(0) > M_PI/2 || isnan(joint_positions.at(0))) {
    ROS_WARN("Kinematic inversion has produced a not accettable result for the first joint: %f", joint_positions.at(0));
    return false;
  }
  joint_positions.at(1) = 2*atan((sqrt(-(81*std::abs(ee_pose.x) + 10000*pow(ee_pose.x,2)*std::abs(ee_pose.x) + 10000*pow(ee_pose.y,2)*std::abs(ee_pose.x) + 10000*pow(ee_pose.z,2)*std::abs(ee_pose.x) - 1800*ee_pose.x*sqrt(pow(ee_pose.x,2) + pow(ee_pose.y,2)))*(10000*pow(ee_pose.x,2)*std::abs(ee_pose.x) - 243*std::abs(ee_pose.x) + 10000*pow(ee_pose.y,2)*std::abs(ee_pose.x) + 10000*pow(ee_pose.z,2)*std::abs(ee_pose.x) - 1800*ee_pose.x*sqrt(pow(ee_pose.x,2) + pow(ee_pose.y,2)))) + 1800*ee_pose.z*std::abs(ee_pose.x))/(std::abs(ee_pose.x)*(10000*pow(ee_pose.x,2) + 10000*pow(ee_pose.y,2) + 10000*pow(ee_pose.z,2) - 81)));
  if (joint_positions.at(1) < - 125*M_PI/180 || joint_positions.at(1) > 125*M_PI/180 || isnan(joint_positions.at(1))) {
    ROS_WARN("Kinematic inversion has produced a not accettable result for the second joint: %f", joint_positions.at(1));
    return false;
  }
  joint_positions.at(2) = -2*atan(sqrt(-(81*std::abs(ee_pose.x) + 10000*pow(ee_pose.x,2)*std::abs(ee_pose.x) + 10000*pow(ee_pose.y,2)*std::abs(ee_pose.x) + 10000*pow(ee_pose.z,2)*std::abs(ee_pose.x) - 1800*ee_pose.x*sqrt(pow(ee_pose.x,2) + pow(ee_pose.y,2)))*(10000*pow(ee_pose.x,2)*std::abs(ee_pose.x) - 243*std::abs(ee_pose.x) + 10000*pow(ee_pose.y,2)*std::abs(ee_pose.x) + 10000*pow(ee_pose.z,2)*std::abs(ee_pose.x) - 1800*ee_pose.x*sqrt(pow(ee_pose.x,2) + pow(ee_pose.y,2))))/(81*std::abs(ee_pose.x) + 10000*pow(ee_pose.x,2)*std::abs(ee_pose.x) + 10000*pow(ee_pose.y,2)*std::abs(ee_pose.x) + 10000*pow(ee_pose.z,2)*std::abs(ee_pose.x) - 1800*ee_pose.x*sqrt(pow(ee_pose.x,2) + pow(ee_pose.y,2))));
  if (joint_positions.at(2) < - 125*M_PI/180 || joint_positions.at(2) > 125*M_PI/180 || isnan(joint_positions.at(2))) {
    ROS_WARN("Kinematic inversion has produced a not accettable result for the third joint: %f", joint_positions.at(2));
    return false;
  }
  return true;
};

PLUGINLIB_EXPORT_CLASS(qb_chain_controllers::ArmV1KinematicController, controller_interface::ControllerBase)