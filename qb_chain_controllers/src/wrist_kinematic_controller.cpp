/***
 *  Software License Agreement: BSD 3-Clause License
 *  
 *  Copyright (c) 2016-2021, qbrobotics®
 *  All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 *  following conditions are met:
 *  
 *  * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *  
 *  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *  
 *  * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 *  USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <qb_chain_controllers/wrist_kinematic_controller.h>

using namespace qb_chain_controllers;

WristKinematicController::WristKinematicController()
  : ik_solver_("wrist_base_frame_link", "wrist_ee_frame_link", "/qbwrist/robot_description", 0.005, 1e-5),
  pos_tol_ (1e-5, 1e-5, 1e-5),
  ang_tol_ (1e3, 1e3, 1e3),
  tolerances_ (KDL::Twist(pos_tol_, ang_tol_)) {
    kinematic_config_ = "wrist";
    valid_ = ik_solver_.getKDLChain(chain_);
    q_.resize(3);
    q_(0) = 0.0;
    q_(1) = 0.0;
    q_(2) = 0.0;
};

WristKinematicController::~WristKinematicController() {};

bool WristKinematicController::forwardKinematics(const std::vector<double> &motor_joints, geometry_msgs::Point &ee_pose) {
  if(!valid_) {
    ROS_ERROR_STREAM("No valid robot model found.");
    return false;
  }
  // variables for solving forward kinematic
  KDL::JntArray joints_positions(3);
  joints_positions(0) = motor_joints.at(0);
  joints_positions(1) = motor_joints.at(1);
  joints_positions(2) = motor_joints.at(2);
  KDL::Frame end_effector_pose;
  KDL::ChainFkSolverPos_recursive fk_solver(chain_);
  int result = fk_solver.JntToCart(joints_positions, end_effector_pose);

  if(result < 0) {
    ROS_WARN_STREAM("Forward Kinematic has produced a no valid result.");
    return false;
  }

  ee_pose.x = end_effector_pose.p(0);
  ee_pose.y = end_effector_pose.p(1);
  ee_pose.z = end_effector_pose.p(2);
  return true;
};

bool WristKinematicController::inverseKinematics(const geometry_msgs::Point &ee_pose, std::vector<double> &joint_positions) {
  if(!valid_) {
    ROS_ERROR_STREAM("No valid robot model found.");
    return false;
  }
  
  KDL::JntArray result;
  // desired position of the e-e
  KDL::Vector target_pos (ee_pose.x, ee_pose.y, ee_pose.z);
  KDL::Frame ee_target_pose(target_pos);
  int is_found = ik_solver_.CartToJnt(q_, ee_target_pose, result, tolerances_); 
  if (type_of_robot_movement_ == "moveL") {
    int number_of_cycles = 0;
    while (!isValidSolution(result, ee_target_pose)) {
      if(++ number_of_cycles >= 25) {
        ROS_WARN_STREAM("No valid solution found with moveL. Try with moveJ or change robot start position (using rviz). ");
        return false;
      }
      is_found = ik_solver_.CartToJnt(q_, ee_target_pose, result, tolerances_); 
    }
  }
  
  if(is_found < 0) {
    ROS_WARN_STREAM("Kinematic inversion has produced a not acceptable result");
    return false;
  }

  joint_positions.resize(3);
  // joint 1, 2 and 3 are setted to the value of ik results
  joint_positions.at(0) = result(0);
  joint_positions.at(1) = result(1);
  joint_positions.at(2) = result(2); 
    
  q_(0) = joint_positions.at(0);
  q_(1) = joint_positions.at(1);
  q_(2) = joint_positions.at(2);
  return true;
};

bool WristKinematicController::isValidSolution(KDL::JntArray jointVariables, KDL::Frame end_effector_pose) {
  double q1 = jointVariables(0);
  double q2 = jointVariables(1);
  double q3 = jointVariables(2);

  double pos_y = end_effector_pose.p(1);
  double pos_z = end_effector_pose.p(2);

  bool condition_joint_1 = ((q1 < 0 && pos_y < 0) || (q1 > 0 && pos_y > 0));
  bool condition_joint_2 = ((q2 < 0 && pos_z > 0) || (q2 > 0 && pos_z < 0));

  return condition_joint_1 && condition_joint_2;
}

PLUGINLIB_EXPORT_CLASS(qb_chain_controllers::WristKinematicController, controller_interface::ControllerBase)