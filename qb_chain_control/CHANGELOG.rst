^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package qb_chain_control
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.0.0 (2022-07-06)
------------------
* Modified .xacro files in order to fit new xacro package version. This commint is suitable with xacro version 1.13.15 -- see https://github.com/ros/xacro/commit/7b5c20907375400cf4db6993a13238e86b38acf3 for more details.

2.2.3 (2021-11-05)
------------------
* changed ee limits in .yaml file
* Changed delta limits

2.2.2 (2021-08-30)
------------------

2.2.1 (2021-08-27)
------------------

2.2.0 (2021-08-26)
------------------
* Modified control frequency for RPi4
* Fixed joint limits and default robot movement.
* Add Gazebo SoftHand and SoftHand 2 Motors plugin skeletons
* Set a better name for fake joint simulation variables
* Fix cmake resource installation
* removed wrong commented parts
* qb_control_chain finds automatically the IP address
* Fix launch
* Add Gazebo support for kinematic chains
* modified shell script for qb control(IP address)
* Add joint limits configuration for robot configs
* Removed files for old configuration. Added scripts for waypoints. Added script to interact with qb control.
* Fixed an error that prevents the usage of WAYPOINT modality. Added .yaml file for parsing wayoints.
* Fix a .launch name and created a new one for loading URDFs when controlling remotely the qb chain.
* Fixed some errors in wrist kinematic and added the last kinematic config(arm_v2).
* Added wrist files to implement the corresponding kinematic (not implemented yet).
* Changed the names of the calsses according to robot configurations described in the guide (qbmove KIT 23/12/2020). for 'arm2' used the library 'trac_ik' for solving inverse kinematic problem.
* fixed some minor bugs
* Added the scripts for 2-axes wrist implementation. The project compiles but the inverse kinematic equations are wrong.
* Added a third kinematic configuration. The open chain kinematics are named arm1 and arm 2.
* added kinematic controller for arm1 configuration
* Added arm3 kinematic controller and custom message to set velocity and stiffness togheter with end-effector position
* Add a launch file in qb_chain_control/launch in order to load 'robot description'

2.1.1 (2019-06-11)
------------------

2.1.0 (2019-05-28)
------------------
* Update demo points and launch settings
* Add a simulator mode to debug joint trajectories
* Fix default control duration of launch files
* Add delta controller

2.0.2 (2018-08-09)
------------------

2.0.1 (2018-08-07)
------------------
* Set safer default values

2.0.0 (2018-06-01)
------------------
* Refactor xacro models and relative launch files

1.0.4 (2017-11-24)
------------------

1.0.3 (2017-06-23)
------------------
* Update cmake version to match Kinetic standards

1.0.2 (2017-06-21)
------------------
* fix cmake settings to solve isolated builds

1.0.1 (2017-06-19)
------------------
* first public release for Kinetic
