#!/bin/bash

RED='\033[0;31m'
L_BLUE='\033[1;34m'
L_GREEN='\033[1;32m'
NC='\033[0m' # No Color


helpFunction()
{
   echo ""
   echo "Usage: $0 -a <kinematic_confiuration>"
   echo -e "\t-a ${L_GREEN}<kinematic_configuration>${NC} is the name of the robot that is controlled by qb control. See the qbmove KIT user guide to find out the corresponding name and ensure that the correct robot configuration is selected."
   echo ""
   exit 1 
}

while getopts "a:" opt
do
   case "$opt" in
      a ) parameter="$OPTARG" ;;
      ? ) helpFunction ;;
   esac
done

if [ -z "$parameter" ]
then
   printf "${RED}You have to specify a robot name. See the qbmove KIT user guide to find out the corresponding name for the robot controlled by qb control.${NC}\n";
   helpFunction
fi

declare -a arr=("arm_v1" "arm_v2" "arm1" "arm2" "wrist" "delta")

valid_name=false

for i in "${arr[@]}"
do
   if [ "$parameter" = "$i" ]; then
      valid_name=true
   fi
done

if $valid_name; then
    printf "${L_BLUE}$parameter robot selected.\n\n${NC}"
else
    printf "${RED}No valid robot name selected. Please refer to qbmove KIT user guide to find out the corresponding name for the robot controlled by qb control.${NC}\n"
    exit 1
fi

QB_IP_ADDRESS="192.168.1.130"
IP_ADDRESS=$(ip route get $QB_IP_ADDRESS | sed -n '/src/{s/.*src *\([^ ]*\).*/\1/p;q}')
echo "qb control IP address: $QB_IP_ADDRESS"
echo "IP address found (PC): $IP_ADDRESS" 
export ROS_IP=$IP_ADDRESS
export ROS_MASTER_URI=http://$QB_IP_ADDRESS:11311

ROBOT_NAME="$parameter"
roslaunch qb_chain_control load_robot_model.launch kinematic_config:=$ROBOT_NAME

rosrun rviz rviz -d ../../qb_chain_description/rviz/qb$ROBOT_NAME.rviz
