^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package qb_chain_msgs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.0.0 (2022-07-06)
------------------

2.2.3 (2021-11-05)
------------------
* Modified package.xml file in order to fix jenkins errors.

2.2.2 (2021-08-30)
------------------
* Fix version tag in some packages.

2.2.1 (2021-08-27)
------------------

2.2.0 (2021-08-26)
------------------
* Added changelog file in msgs package.
* Fix cmake resource installation
* Update license
* Fixed some errors in wrist kinematic and added the last kinematic config(arm_v2).
* Wrist Kinematic implemented.
* Changed the names of the calsses according to robot configurations described in the guide (qbmove KIT 23/12/2020). for 'arm2' used the library 'trac_ik' for solving inverse kinematic problem.
* fixed some minor bugs
* Added arm3 kinematic controller and custom message to set velocity and stiffness togheter with end-effector position
