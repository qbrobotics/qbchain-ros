^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package qb_chain_description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.0.0 (2022-07-06)
------------------
* Modified .xacro files in order to fit new xacro package version. This commint is suitable with xacro version 1.13.15 -- see https://github.com/ros/xacro/commit/7b5c20907375400cf4db6993a13238e86b38acf3 for more details.

2.2.3 (2021-11-05)
------------------

2.2.2 (2021-08-30)
------------------

2.2.1 (2021-08-27)
------------------

2.2.0 (2021-08-26)
------------------
* Fix qbchain flange collisions parse
* Fix cmake resource installation
* Add Gazebo support for kinematic chains
* Removed files for old configuration. Added scripts for waypoints. Added script to interact with qb control.
* Fixed some errors in wrist kinematic and added the last kinematic config(arm_v2).
* Wrist Kinematic implemented.
* Added wrist files to implement the corresponding kinematic (not implemented yet).
* Changed the names of the calsses according to robot configurations described in the guide (qbmove KIT 23/12/2020). for 'arm2' used the library 'trac_ik' for solving inverse kinematic problem.
* fixed some minor bugs
* Deleted the files used by VisualStudio Code.
* Added the scripts for 2-axes wrist implementation. The project compiles but the inverse kinematic equations are wrong.
* Added a third kinematic configuration. The open chain kinematics are named arm1 and arm 2.
* added kinematic controller for arm1 configuration
* Added arm3 kinematic controller and custom message to set velocity and stiffness togheter with end-effector position

2.1.1 (2019-06-11)
------------------

2.1.0 (2019-05-28)
------------------
* Fix description values
* Add delta URDF model

2.0.2 (2018-08-09)
------------------

2.0.1 (2018-08-07)
------------------

2.0.0 (2018-06-01)
------------------
* Refactor xacro models and relative launch files

1.0.4 (2017-11-24)
------------------

1.0.3 (2017-06-23)
------------------
* Update cmake version to match Kinetic standards

1.0.2 (2017-06-21)
------------------

1.0.1 (2017-06-19)
------------------
* first public release for Kinetic
